#!/bin/bash

#SBATCH --tasks-per-node=1
#SBATCH --exclusive
#SBATCH --cpus-per-task=28
#SBATCH --constraint=BDW28
#SBATCH -t 3:00:00
#SBATCH -N 2

export NMAD_IBVERBS_RCACHE=1 # define for MadMPI

module load hwloc/2.5.0
module load pm2-madmpi

echo "**** Running on $SLURM_JOB_NUM_NODES node(s) ($SLURM_JOB_NODELIST) ****"

node_nb_cores=$(srun -N 1 -n 1 hwloc-calc all -N core)
node_nb_numa=$(srun -N 1 -n 1 hwloc-calc all -N node)

srun -N 1 -n 1 lstopo --of xml > topology.xml

echo "**** Found $node_nb_numa NUMA nodes and $node_nb_cores cores per node ****"

mkdir memset_nt
cd memset_nt

for i_numa_comp in $(seq $node_nb_numa)
do
        real_i_numa_comp=$((i_numa_comp-1))
        for i_numa_comm in $(seq $node_nb_numa)
        do 
                real_i_numa_comm=$((i_numa_comm-1))
                echo "** Running with comp_numa_node=${real_i_numa_comp} and comm_numa_node=${real_i_numa_comm} **"
                date
                for i in $(seq $node_nb_cores)
                do
                        echo $i
                        mpirun -timeout 600 \
                                -DOMP_NUM_THREADS=$i -DOMP_PROC_BIND=true -DOMP_PLACES=cores \
                                hwloc-bind --cpubind core:0-$((i-1)) \
                                ~/bench_openmp_madmpi \
                                --one_computing_rank --per_thread --only_pong --bench=bandwidth \
                                --bind_memory_comp=${real_i_numa_comp} --bind_memory_comm=${real_i_numa_comm} \
                                --compute_bench=memset --nt --throughput \
                                > comp_${real_i_numa_comp}_comm_${real_i_numa_comm}_${i}_threads.out
                done
        done
done
date
