#!/bin/bash

export NMAD_IBVERBS_RCACHE=1 # define for MadMPI

node_nb_cores=$(hwloc-calc all -N core)
node_nb_numa=$(hwloc-calc all -N node)

lstopo --of xml > topology.xml

echo "**** Found $node_nb_numa NUMA nodes and $node_nb_cores cores per node ****"

mkdir memset_nt
cd memset_nt

for i_numa_comp in $(seq $node_nb_numa)
do
        real_i_numa_comp=$((i_numa_comp-1))
        for i_numa_comm in $(seq $node_nb_numa)
        do
                real_i_numa_comm=$((i_numa_comm-1))
                echo "** Running with comp_numa_node=${real_i_numa_comp} and comm_numa_node=${real_i_numa_comm} **"
                date
                for i in $(seq $node_nb_cores)
                do
                        echo $i
                        mpirun -n 2 -nodelist billy0,billy1 -timeout 600 \
                                -DOMP_NUM_THREADS=$i -DOMP_PROC_BIND=true -DOMP_PLACES=cores \
                                hwloc-bind --cpubind core:0-$((i-1)) \
                                ~/memory-bottleneck/build/src/bench_openmp \
                                --one_computing_rank --per_thread --only_pong --bench=bandwidth \
                                --bind_memory_comp=${real_i_numa_comp} --bind_memory_comm=${real_i_numa_comm} \
                                --compute_bench=memset --nt --throughput \
                                > comp_${real_i_numa_comp}_comm_${real_i_numa_comm}_${i}_threads.out
                done
        done
done
date
