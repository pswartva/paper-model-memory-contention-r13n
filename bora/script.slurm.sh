#!/bin/bash

#SBATCH --tasks-per-node=1
#SBATCH --exclusive
#SBATCH -N 2

export GUIX_PACKAGE_PATH=../guix/guix-modules:${GUIX_PACKAGE_PATH}
guix_channels_file=../guix/guix-channels.scm

echo "**** Running on $SLURM_JOB_NUM_NODES node(s) ($SLURM_JOB_NODELIST) ****"

node_nb_cores=$(srun -N 1 -n 1 guix time-machine --channels=$guix_channels_file -- shell --pure --preserve=^SLURM hwloc -- hwloc-calc all -N core)
node_nb_numa=$(srun -N 1 -n 1 guix time-machine --channels=$guix_channels_file -- shell --pure --preserve=^SLURM hwloc -- hwloc-calc all -N node)

echo "**** Found $node_nb_numa NUMA nodes and $node_nb_cores cores per node ****"

srun -N 1 -n 1 guix time-machine --channels=$guix_channels_file -- shell --pure --preserve=^SLURM hwloc -- lstopo --of xml > topology.xml

date
guix time-machine --channels=$guix_channels_file -- shell --pure --preserve=^SLURM coreutils slurm@19 memory-contention --with-input=openmpi=nmad-mini -- ./exp.sh $node_nb_cores $node_nb_numa
date
